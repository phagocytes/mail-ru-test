//
//  Helpers.m
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import "Helpers.h"

@implementation UIViewController(embed)
- (void)embedChildViewController:(UIViewController *)vc toContainerView:(UIView *)view
{
    vc.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:vc.view];
    
    NSDictionary * views = @{ @"v": vc.view };
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[v]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[v]|" options:0 metrics:nil views:views]];
    
    [self addChildViewController:vc];
    [vc didMoveToParentViewController:self];
}
@end


@implementation NSObject(apply)
- (instancetype)apply:(void(^)(id object))block
{
    NSParameterAssert(block);
    block(self);
    return self;
}
+ (instancetype)cast:(id)object
{
    if ([object isKindOfClass:self]) return object;
    return nil;
}
@end


CGSize albumThumbnailSize()
{
    return CGSizeMake(30, 30);
}

CGSize photoSmallSize()
{
    return CGSizeMake(300, 300);
}

UIImage * placeholderAlbum()
{
    return [UIImage imageNamed:@"placeholder_album"];
}
