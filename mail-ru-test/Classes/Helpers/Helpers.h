//
//  Helpers.h
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIViewController(embed)
- (void)embedChildViewController:(UIViewController *)vc toContainerView:(UIView *)view;
@end

@interface NSObject(apply)
- (instancetype)apply:(void(^)(id object))block;
+ (instancetype)cast:(id)object;
@end

CGSize albumThumbnailSize();
CGSize photoSmallSize();

UIImage * placeholderAlbum();
