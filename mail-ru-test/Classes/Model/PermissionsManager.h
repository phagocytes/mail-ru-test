//
//  PermissionsManager.h
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import <Foundation/Foundation.h>

@interface PermissionsManager : NSObject

+ (void)checkPermissions:(dispatch_block_t)onSuccess onFail:(dispatch_block_t)onFail;

@end
