//
//  PermissionsManager.m
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import "PermissionsManager.h"

@implementation PermissionsManager

+ (void)checkPermissions:(dispatch_block_t)onSuccess onFail:(dispatch_block_t)onFail
{
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status)
        {
            case PHAuthorizationStatusNotDetermined:
            case PHAuthorizationStatusDenied:
            case PHAuthorizationStatusRestricted: {
                if (onFail) onFail();
                break;
            }
                
            case PHAuthorizationStatusAuthorized: {
                if (onSuccess) onSuccess();
                break;
            }
        }
    }];
}

@end
