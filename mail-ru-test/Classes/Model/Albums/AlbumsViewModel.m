//
//  AlbumsViewModel.m
//  mail-ru-test
//
//  Created by 5craft on 17/11/2016.
//
//

#import "AlbumsViewModel.h"
#import "Album.h"

@interface AlbumsViewModel()

@property (weak) id<AlbumsViewModelDelegate> delegate;

@property (nonatomic) NSArray * albums;

@end


@implementation AlbumsViewModel

+ (instancetype)newWithDelegate:(id<AlbumsViewModelDelegate>)delegate
{
    return [[AlbumsViewModel new] apply:^(AlbumsViewModel * vm) {
        vm.delegate = delegate;
    }];
}


#pragma mark - Public

- (void)loadAlbums
{
    [self.delegate albumsViewModelWillLoadAlbums];
    
    __weak typeof(self) this = self;
    dispatch_block_t fetchBlock = ^{
        [[PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAny options:nil] apply:^(PHFetchResult * result) {
            NSMutableArray * albums = [NSMutableArray array];
            for (PHAssetCollection * collection in result) {
                Album * album = [Album newWithTitle:collection.localizedTitle assetCollection:collection];
                [albums addObject:album];
            }
            this.albums = [albums copy];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [this.delegate albumsViewModelDidLoadAlbums];
            });
        }];
    };
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), fetchBlock);
}

- (NSInteger)numberOfAlbums
{
    return self.albums.count;
}

- (Album *)albumAtIndex:(NSInteger)idx
{
    return self.albums[idx];
}

@end
