//
//  AlbumsViewModel.h
//  mail-ru-test
//
//  Created by 5craft on 17/11/2016.
//
//

#import <Foundation/Foundation.h>

@class Album;

@protocol AlbumsViewModelDelegate <NSObject>
- (void)albumsViewModelWillLoadAlbums;
- (void)albumsViewModelDidLoadAlbums;
@end

@interface AlbumsViewModel : NSObject

+ (instancetype)newWithDelegate:(id<AlbumsViewModelDelegate>)delegate;

- (void)loadAlbums;

- (NSInteger)numberOfAlbums;
- (Album *)albumAtIndex:(NSInteger)idx;

@end
