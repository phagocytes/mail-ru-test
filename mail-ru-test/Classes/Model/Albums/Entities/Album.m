//
//  Album.m
//  mail-ru-test
//
//  Created by 5craft on 17/11/2016.
//
//

#import "Album.h"
#import "Photo.h"

@interface Album()

@property (nonatomic) PHAssetCollection * assetCollection;
@property (nonatomic) NSInteger photosCount;
@property (nonatomic) UIImage * icon;
@property (nonatomic) NSArray * photos;

@end

@implementation Album

+ (instancetype)newWithTitle:(NSString *)title assetCollection:(PHAssetCollection *)assetCollection
{
    return [[Album new] apply:^(Album * album) {
        album->_title = title;
        album.assetCollection = assetCollection;
    }];
}

- (void)setAssetCollection:(PHAssetCollection *)assetCollection
{
    if (_assetCollection == assetCollection) {
        return;
    }
    
    _assetCollection = assetCollection;
    [self fetchData:nil];
}

- (void)fetchData:(dispatch_block_t)competion
{
    PHFetchOptions * options = [PHFetchOptions new];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
    options.predicate = [NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeImage];

    __weak typeof(self) this = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        PHFetchResult * fetchResult = [PHAsset fetchAssetsInAssetCollection:self.assetCollection options:options];
        if (fetchResult.count < 1) {
            return;
        }
        
        NSMutableArray * photos = [NSMutableArray array];
        for (PHAsset * asset in fetchResult) {
            Photo * photo = [Photo newWithAsset:asset];
            [photos addObject:photo];
        }
        this.photos = photos;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [this.delegate albumDidFinishFetchingAssests:this];
            [[PHCachingImageManager defaultManager] requestImageForAsset:[fetchResult lastObject] targetSize:albumThumbnailSize() contentMode:PHImageContentModeAspectFill options:nil resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                this.icon = result;
                [this.delegate albumDidFinishFetchingIcon:this];
                if (competion) competion();
            }];
        });
    });
}

- (UIImage *)icon
{
    return _icon ? : placeholderAlbum();
}


@end
