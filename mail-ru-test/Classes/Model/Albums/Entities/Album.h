//
//  Album.h
//  mail-ru-test
//
//  Created by 5craft on 17/11/2016.
//
//

#import <Foundation/Foundation.h>

@class PHAssetCollection, Album;

@protocol AlbumDelegate <NSObject>
- (void)albumDidFinishFetchingAssests:(Album *)album;
- (void)albumDidFinishFetchingIcon:(Album *)album;
@end

@interface Album : NSObject

@property (readonly) NSString * title;
@property (readonly) UIImage * icon;
@property (readonly) NSArray * photos;

@property (weak) id<AlbumDelegate> delegate;

+ (instancetype)newWithTitle:(NSString *)title assetCollection:(PHAssetCollection *)assetCollection;

- (void)fetchData:(dispatch_block_t)completion;

@end
