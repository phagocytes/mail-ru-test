//
//  Photo.m
//  mail-ru-test
//
//  Created by 5craft on 17/11/2016.
//
//

#import "Photo.h"
#import "PhotoImageLoader.h"

@interface Photo()

@property (nonatomic) UIImage * smallImage;
@property (nonatomic) UIImage * largeImage;

@end

@implementation Photo

+ (instancetype)newWithAsset:(PHAsset *)asset
{
    return [[Photo new] apply:^(Photo * photo) {
        photo->_asset = asset;
        photo->_imageLoader = [PhotoImageLoader newWithAsset:asset];
    }];
}

@end
