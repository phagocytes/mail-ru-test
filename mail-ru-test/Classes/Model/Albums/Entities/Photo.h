//
//  Photo.h
//  mail-ru-test
//
//  Created by 5craft on 17/11/2016.
//
//

#import <Foundation/Foundation.h>

@class PHAsset, PhotoImageLoader;

@interface Photo : NSObject

@property (readonly) PHAsset * asset;
@property (readonly) PhotoImageLoader * imageLoader;

+ (instancetype)newWithAsset:(PHAsset *)asset;

@end
