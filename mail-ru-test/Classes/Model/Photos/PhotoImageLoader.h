//
//  PhotoImageLoader.h
//  mail-ru-test
//
//  Created by 5craft on 17/11/2016.
//
//

#import <Foundation/Foundation.h>

@class PHAsset;

typedef void(^ImageBlock)(UIImage * image);

typedef NS_ENUM(NSInteger, PhotoImageKind)
{
    PhotoImageKindThumbnail,
    PhotoImageKindFull
};

@interface PhotoImageLoader : NSObject

@property (copy) dispatch_block_t willReloadCallback;
@property (copy) ImageBlock didReloadCallback;

+ (instancetype)newWithAsset:(PHAsset *)asset;

- (void)loadImageKind:(PhotoImageKind)kind forClient:(id)client onSuccess:(ImageBlock)onSuccess;

@end
