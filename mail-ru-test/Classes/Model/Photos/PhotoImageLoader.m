//
//  PhotoImageLoader.m
//  mail-ru-test
//
//  Created by 5craft on 17/11/2016.
//
//

#import "PhotoImageLoader.h"
#import "PhotoSizeProvider.h"

@interface PhotoImageLoader()

@property (nonatomic) PHAsset * asset;
@property (nonatomic) BOOL isFetching;
@property (copy) ImageBlock onSuccess;
@property (weak) id client;

@end

@implementation PhotoImageLoader

+ (instancetype)newWithAsset:(PHAsset *)asset
{
    return [[PhotoImageLoader new] apply:^(PhotoImageLoader * loader) {
        loader.asset = asset;
    }];
}

- (void)loadImageKind:(PhotoImageKind)kind forClient:(id)client onSuccess:(ImageBlock)onSuccess
{
    if (self.client == client) return;
    
    self.client = client;
    self.onSuccess = onSuccess;
    if (self.isFetching) return;
    
    CGSize size = CGSizeZero;
    if (kind == PhotoImageKindThumbnail) {
        size = [PhotoSizeProvider sizeForSmallPhoto];
    } else if (kind == PhotoImageKindFull) {
        size = [PhotoSizeProvider sizeForFullPhotoFromAsset:self.asset];
    }
    
    __weak typeof(self) this = self;
    void (^fetchingCompetion)(UIImage * _Nullable result, NSDictionary * _Nullable info) = ^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
        this.isFetching = NO;
        [this _callSuccess:result];
    };
    
    self.isFetching = YES;
    [[PHCachingImageManager defaultManager] requestImageForAsset:self.asset
                                                      targetSize:size
                                                     contentMode:PHImageContentModeAspectFill
                                                         options:nil
                                                   resultHandler:fetchingCompetion];
}

- (void)_callSuccess:(UIImage *)image
{
    if (self.onSuccess) self.onSuccess(image);
    self.client = nil;
}

@end
