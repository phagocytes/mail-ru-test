//
//  PhotosViewModel.h
//  mail-ru-test
//
//  Created by 5craft on 17/11/2016.
//
//

#import <Foundation/Foundation.h>

@class Album, Photo;

@protocol PhotosViewModelDelegate <NSObject>
- (void)photosViewModelWillReload;
- (void)photosViewModelDidReload;
@end

@protocol PhotosViewModel <NSObject>

@property (readonly) Album * album;
@property (readonly) NSInteger chosenPhotoIndex;

+ (instancetype)newWithAlbum:(Album *)album;

- (void)choosePhotoAtIndex:(NSInteger)idx;
- (Photo *)photoAtIndex:(NSInteger)idx;
- (NSInteger)photosCount;

- (void)reload;

@end



@interface DefaultPhotosViewModel : NSObject <PhotosViewModel>
@property (weak) id<PhotosViewModelDelegate> delegate;
@end

@interface ThumbnailPhotosViewModel : DefaultPhotosViewModel
@end

@interface FullPhotosViewModel : DefaultPhotosViewModel
@end
