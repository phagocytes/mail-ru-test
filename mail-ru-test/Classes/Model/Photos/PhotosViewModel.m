//
//  PhotosViewModel.m
//  mail-ru-test
//
//  Created by 5craft on 17/11/2016.
//
//

#import "PhotosViewModel.h"
#import "Album.h"
#import "Photo.h"
#import "PhotoImageLoader.h"

#import "PhotoSizeProvider.h"

@interface DefaultPhotosViewModel() <AlbumDelegate>

@property (nonatomic) NSInteger chosenPhotoIndex;
@property (nonatomic) Album * album;

@end


@implementation DefaultPhotosViewModel


#pragma mark - Init

+ (instancetype)newWithAlbum:(Album *)album
{
    return [[self.class new] apply:^(DefaultPhotosViewModel * model) {
        model.album = album;
    }];
}


#pragma mark - <PhotosViewModel>

- (void)choosePhotoAtIndex:(NSInteger)idx
{
    self.chosenPhotoIndex = idx;
}

- (Photo *)photoAtIndex:(NSInteger)idx
{
    return self.album.photos[idx];
}

- (NSInteger)photosCount
{
    return self.album.photos.count;
}

- (void)reload
{
    [self.delegate photosViewModelWillReload];
    
    __weak typeof(self) this = self;
    [self.album fetchData:^{
        [this.delegate photosViewModelDidReload];
    }];
}


#pragma mark - <AlbumDelegate>

- (void)albumDidFinishFetchingAssests:(Album *)album
{
    
}

- (void)albumDidFinishFetchingIcon:(Album *)album
{
    
}

@end




@implementation ThumbnailPhotosViewModel
@end


@implementation FullPhotosViewModel
@end
