//
//  AppDelegate.h
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

