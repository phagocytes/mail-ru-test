//
//  AppDelegate.m
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import "AppDelegate.h"
#import "AlbumsViewController.h"
#import "PermissionsManager.h"

@interface AppDelegate ()

@property BOOL isAlertPresenting;

@end


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:[AlbumsViewController new]];
    self.window.backgroundColor = [UIColor blackColor];
    [self.window makeKeyAndVisible];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {

}


- (void)applicationDidEnterBackground:(UIApplication *)application {

}


- (void)applicationWillEnterForeground:(UIApplication *)application {

}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self checkPermissions];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    
}

- (void)checkPermissions
{
    __weak typeof(self) this = self;
    [PermissionsManager checkPermissions:^{
        if (!this.isAlertPresenting) {
            return;
        }
        
        [this.window.rootViewController dismissViewControllerAnimated:YES completion:^{
            this.isAlertPresenting = NO;
        }];
    } onFail:^{
        if (this.isAlertPresenting) {
            return;
        }
        
        [[UIAlertController alertControllerWithTitle:@"Внимание!"
                                             message:@"Разрешите использование фото-библиотеки для работы приложения"
                                      preferredStyle:UIAlertControllerStyleAlert] apply:^(UIAlertController * ac) {
            [this.window.rootViewController presentViewController:ac animated:YES completion:^{
                this.isAlertPresenting = YES;
            }];
        }];
    }];
}


@end
