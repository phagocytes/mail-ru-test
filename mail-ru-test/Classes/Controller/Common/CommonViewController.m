//
//  CommonViewController.m
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import "CommonViewController.h"
#import "LoadingViewController.h"

@interface CommonViewController ()

@property (nonatomic) LoadingViewController * loadingVC;

@end

@implementation CommonViewController


#pragma mark - Lifecycle

- (void)loadView
{
    self.view = [UIView new];
    [self setupUI];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.loadingVC = [LoadingViewController new];
    [self embedChildViewController:self.loadingVC toContainerView:self.view];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateNavbar];
}


#pragma mark - UI

- (void)setupUI
{
    UIView * container = self.view;
    container.backgroundColor = [UIColor whiteColor];
}

- (void)updateNavbar
{
    if (!self.navigationController) {
        return;
    }
    
    if (self.navigationController.topViewController == self.navigationController.viewControllers[0]) {
        return;
    }
    
    UIBarButtonItem * backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Назад" style:UIBarButtonItemStylePlain target:self action:@selector(goBack)];
    self.navigationItem.leftBarButtonItem = backButtonItem;
}


#pragma mark - Methods

- (void)showLoading
{
    [self.loadingVC show];
}

- (void)hideLoading
{
    [self.loadingVC hide];
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
