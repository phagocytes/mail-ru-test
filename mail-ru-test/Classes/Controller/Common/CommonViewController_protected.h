//
//  CommonViewController_protected.h
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import "CommonViewController.h"

@interface CommonViewController ()

- (void)setupUI;
- (void)showLoading;
- (void)hideLoading;

@end
