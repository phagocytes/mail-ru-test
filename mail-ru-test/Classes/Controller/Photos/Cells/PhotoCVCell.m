//
//  PhotoCVCell.m
//  mail-ru-test
//
//  Created by 5craft on 17/11/2016.
//
//

#import "PhotoCVCell.h"
#import "Photo.h"
#import "PhotoImageLoader.h"

@interface PhotoCVCell()

@property (nonatomic) UIActivityIndicatorView * activityIndicator;
@property (nonatomic) UIImageView * photoImageView;

@end


@implementation PhotoCVCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self == nil) {
        return nil;
    }
    
    UIView * container = self;
    
    self.photoImageView = [[UIImageView new] apply:^(UIImageView * iv) {
        iv.clipsToBounds = YES;
        iv.contentMode = UIViewContentModeScaleAspectFill;
        iv.translatesAutoresizingMaskIntoConstraints = NO;
        [container addSubview:iv];
        
        CGFloat margin = [self isKindOfClass:FullPhotoCVCell.class] ? 10 : 0;
        NSDictionary * views = NSDictionaryOfVariableBindings(iv);
        NSDictionary * metrics = @{ @"margin": @(margin) };
        [container addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(margin)-[iv]-(margin)-|" options:0 metrics:metrics views:views]];
        [container addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(margin)-[iv]-(margin)-|" options:0 metrics:metrics views:views]];
    }];
    
    self.activityIndicator = [[UIActivityIndicatorView new] apply:^(UIActivityIndicatorView * indicator) {
        indicator.hidesWhenStopped = YES;
        indicator.color = [UIColor blackColor];
        indicator.translatesAutoresizingMaskIntoConstraints = NO;
        
        [container addSubview:indicator];
        [container addConstraint:[NSLayoutConstraint constraintWithItem:container
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:indicator
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1.
                                                               constant:0.]];
        
        [container addConstraint:[NSLayoutConstraint constraintWithItem:container
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:indicator
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1.
                                                               constant:0.]];
    }];
    
    return self;
}

- (void)setPhoto:(Photo *)photo
{
    if (_photo == photo) {
        return;
    }
    
    _photo = photo;
    [self update];
}

- (void)update
{
    self.photoImageView.image = nil;
    
    __weak typeof(self) this = self;
    self.photo.imageLoader.willReloadCallback = ^{
        this.photoImageView.image = nil;
        [this.activityIndicator startAnimating];
    };
    
    self.photo.imageLoader.didReloadCallback = ^(UIImage * image){
        this.photoImageView.image = image;
        [this.activityIndicator stopAnimating];
    };
    
    PhotoImageKind kind = [self isKindOfClass:FullPhotoCVCell.class] ? PhotoImageKindFull : PhotoImageKindThumbnail;
    
    [self.activityIndicator startAnimating];
    PhotoImageLoader * loader = self.photo.imageLoader;
    [loader loadImageKind:kind forClient:self onSuccess:^(UIImage *image) {
        if (loader != this.photo.imageLoader) return;
        
        [this.activityIndicator stopAnimating];
        this.photoImageView.image = image;
    }];
}

@end




@implementation FullPhotoCVCell
@end

@implementation ThumbnailPhotoCVCell
@end
