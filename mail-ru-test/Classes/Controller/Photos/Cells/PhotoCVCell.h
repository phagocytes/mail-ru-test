//
//  PhotoCVCell.h
//  mail-ru-test
//
//  Created by 5craft on 17/11/2016.
//
//

#import <UIKit/UIKit.h>

static NSString * const kReuseIdentifierCVCellThumbnailPhoto = @"thumbnailPhotoCVCell";
static NSString * const kReuseIdentifierCVCellFullPhoto = @"fullPhotoCVCell";

@class Photo;

@interface PhotoCVCell : UICollectionViewCell

@property (nonatomic) Photo * photo;

@end


@interface ThumbnailPhotoCVCell : PhotoCVCell
@end

@interface FullPhotoCVCell : PhotoCVCell
@end
