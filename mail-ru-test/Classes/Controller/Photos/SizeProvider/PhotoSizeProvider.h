//
//  SmallPhotosSizeProvider.h
//  mail-ru-test
//
//  Created by 5craft on 17/11/2016.
//
//

#import <Foundation/Foundation.h>

extern const CGFloat kInsetBetweenSmallPhotos;
extern const NSInteger kSmallPhotosPerLine;

@interface PhotoSizeProvider : NSObject

+ (CGSize)sizeForSmallPhoto;
+ (CGSize)sizeForFullPhotoFromAsset:(PHAsset *)asset;

@end
