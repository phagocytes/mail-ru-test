//
//  SmallPhotosSizeProvider.m
//  mail-ru-test
//
//  Created by 5craft on 17/11/2016.
//
//

#import "PhotoSizeProvider.h"

const CGFloat kInsetBetweenSmallPhotos = 10.;
const NSInteger kSmallPhotosPerLine = 4;

@implementation PhotoSizeProvider

+ (CGSize)sizeForSmallPhoto
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    CGFloat inset = kInsetBetweenSmallPhotos;
    NSInteger count = kSmallPhotosPerLine;
    CGFloat width = (screenWidth - inset * (count + 1))/count;
    
    return CGSizeMake(width, width);
}

+ (CGSize)sizeForFullPhotoFromAsset:(PHAsset *)asset
{
    return CGSizeMake(asset.pixelWidth, asset.pixelHeight);
}

@end
