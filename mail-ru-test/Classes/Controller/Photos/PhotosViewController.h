//
//  PhotosViewController.h
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import "CommonViewController.h"

@class DefaultPhotosViewModel;

@interface PhotosViewController : CommonViewController

+ (instancetype)newWithViewModel:(DefaultPhotosViewModel *)model;

@end
