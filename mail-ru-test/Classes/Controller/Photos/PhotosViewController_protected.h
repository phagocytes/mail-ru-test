//
//  PhotosViewController_protected.h
//  mail-ru-test
//
//  Created by 5craft on 17/11/2016.
//
//

#import "PhotosViewController.h"
#import "CommonViewController_protected.h"
#import "PhotosViewModel.h"

#import "PhotoCVCell.h"

@interface PhotosViewController () <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>

@property (nonatomic) UICollectionViewController * cvController;
@property (nonatomic) DefaultPhotosViewModel * viewModel;

@property (copy) void(^viewWillTransitionToSizeCallback)();
@property (copy) void(^transitionCompletionCallback)();
@property (copy) void(^viewWillAppearCallback)();

+ (NSString *)reuseIdentifierForCVCell;
- (void)reloadData;

@end
