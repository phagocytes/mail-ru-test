//
//  PhotosViewController.m
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import "PreviewViewController.h"
#import "PhotosViewController_protected.h"

#import "PhotoSizeProvider.h"


@interface PhotosViewController() <PhotosViewModelDelegate>
@end


@implementation PhotosViewController

+ (instancetype)newWithViewModel:(DefaultPhotosViewModel *)model
{
    return [[self.class new] apply:^(PhotosViewController * vc) {
        vc.viewModel = model;
        model.delegate = vc;
    }];
}


#pragma mark - UI

- (void)setupUI
{
    [super setupUI];
    
    self.title = @"Фотографии";
    
    [self setupCollectionViewController];
    
    UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout cast:self.cvController.collectionView.collectionViewLayout];
    
    CGFloat inset = kInsetBetweenSmallPhotos;
    layout.sectionInset = UIEdgeInsetsMake(inset, inset, inset, inset);
    layout.minimumLineSpacing = inset;
    layout.minimumInteritemSpacing = inset;
}


#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    __weak typeof(self) this = self;
    self.viewWillTransitionToSizeCallback = ^{
         [this reloadData];
    };
    
    self.viewWillAppearCallback = ^{
        [this reloadData];
    };
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.viewWillAppearCallback) self.viewWillAppearCallback();
}


#pragma mark - <UICollectionViewDelegate>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.viewModel photosCount];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [[collectionView dequeueReusableCellWithReuseIdentifier:[self.class reuseIdentifierForCVCell] forIndexPath:indexPath] apply:^(PhotoCVCell * cell) {
        cell.photo = [self.viewModel photoAtIndex:indexPath.item];
    }];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [PhotoSizeProvider sizeForSmallPhoto];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    
    FullPhotosViewModel * viewModel = [FullPhotosViewModel newWithAlbum:self.viewModel.album];
    [viewModel choosePhotoAtIndex:indexPath.item];
    [self.navigationController pushViewController:[PreviewViewController newWithViewModel:viewModel] animated:YES];
}


#pragma mark - <PhotosViewModelDelegate>

- (void)photosViewModelWillReload
{
    [self showLoading];
}

- (void)photosViewModelDidReload
{
    [self hideLoading];
    [self.cvController.collectionView reloadData];
}


#pragma mark - Protected

+ (NSString *)reuseIdentifierForCVCell
{
    return kReuseIdentifierCVCellThumbnailPhoto;
}

- (void)reloadData
{
    [self.viewModel reload];
}


#pragma mark -

- (void)setupCollectionViewController
{
    self.cvController = [[UICollectionViewController alloc] initWithCollectionViewLayout:[UICollectionViewFlowLayout new]];
    self.cvController.collectionView.delegate = self;
    self.cvController.collectionView.dataSource = self;
    self.cvController.collectionView.backgroundColor = [UIColor whiteColor];
    
    [self embedChildViewController:self.cvController toContainerView:self.view];
    
    [self.cvController.collectionView registerClass:[ThumbnailPhotoCVCell class] forCellWithReuseIdentifier:kReuseIdentifierCVCellThumbnailPhoto];
    [self.cvController.collectionView registerClass:[FullPhotoCVCell class] forCellWithReuseIdentifier:kReuseIdentifierCVCellFullPhoto];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    if (self.viewWillTransitionToSizeCallback) self.viewWillTransitionToSizeCallback();
    
    __weak typeof(self) this = self;
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
    } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        if (this.transitionCompletionCallback) this.transitionCompletionCallback();
    }];
}

@end
