//
//  LoadingViewController.m
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import "LoadingViewController.h"

const NSTimeInterval kFadeAnimationDuration = 0.4;
const CGFloat kFadeAnimationAlpha = 0.5;

@interface LoadingViewController ()

@property BOOL isAnimating;
@property UIActivityIndicatorView * indicator;

@end

@implementation LoadingViewController

- (void)loadView
{
    self.view = [UIView new];
    self.view.backgroundColor = [UIColor greenColor];
    
    self.indicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] apply:^(UIActivityIndicatorView * indicator) {
        indicator.color = [UIColor blackColor];
        indicator.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.view addSubview:indicator];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:indicator
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1.
                                                               constant:0.]];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:indicator
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1.
                                                               constant:0.]];
    }];
}

- (void)didMoveToParentViewController:(UIViewController *)parent
{
    self.view.alpha = 0;
    self.view.hidden = YES;
}

- (void)show
{
    if (self.isAnimating) {
        return;
    }

    self.view.hidden = NO;
    self.isAnimating = YES;
    [self.indicator startAnimating];
    
    [UIView animateWithDuration:kFadeAnimationDuration animations:^{
        self.view.alpha = kFadeAnimationAlpha;
    }];
}

- (void)hide
{
    [UIView animateWithDuration:kFadeAnimationDuration animations:^{
        self.view.alpha = 0.;
    } completion:^(BOOL finished) {
        self.view.hidden = YES;
        self.isAnimating = NO;
        [self.indicator stopAnimating];
    }];
}

@end
