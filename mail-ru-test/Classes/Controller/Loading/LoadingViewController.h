//
//  LoadingViewController.h
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import <UIKit/UIKit.h>

@interface LoadingViewController : UIViewController

- (void)show;
- (void)hide;

@end
