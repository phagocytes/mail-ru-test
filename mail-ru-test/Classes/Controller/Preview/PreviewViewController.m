//
//  PreviewViewController.m
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import "PreviewViewController.h"
#import "PhotosViewController_protected.h"

@interface PreviewViewController ()

@property BOOL didLayoutSubviewsOnce;

@end


@implementation PreviewViewController

- (void)setupUI
{
    [super setupUI];
    
    self.title = @"Фото";
    self.cvController.collectionView.pagingEnabled = YES;
//    self.automaticallyAdjustsScrollViewInsets = NO;
    
    UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout cast:self.cvController.collectionView.collectionViewLayout];
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    layout.minimumLineSpacing = 0.;
    layout.minimumInteritemSpacing = 0.;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UICollectionView * cv = self.cvController.collectionView;
    NSInteger __block currentIndex = 0;
    
    self.viewWillTransitionToSizeCallback = ^{
        CGPoint currentOffset = cv.contentOffset;
        currentIndex = currentOffset.x / cv.frame.size.width;
    };
    
    self.transitionCompletionCallback = ^{
        [cv.collectionViewLayout invalidateLayout];
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:currentIndex inSection:0];
        [cv scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
        
        [UIView animateWithDuration:0.125f animations:^{
            [cv setAlpha:1.];
        }];
    };
    
    self.viewWillAppearCallback = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSIndexPath * chosenIndexPath = [NSIndexPath indexPathForItem:self.viewModel.chosenPhotoIndex inSection:0];;
    [self.cvController.collectionView scrollToItemAtIndexPath:chosenIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if (self.didLayoutSubviewsOnce) return;
    
    self.didLayoutSubviewsOnce = YES;
    [self reloadData];
}


#pragma mark - <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.bounds.size.width, collectionView.bounds.size.height + collectionView.contentOffset.y);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - Protected

+ (NSString *)reuseIdentifierForCVCell
{
    return kReuseIdentifierCVCellFullPhoto;
}


@end
