//
//  AlbumsViewController.m
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import "AlbumsViewController.h"
#import "PhotosViewController.h"
#import "CommonViewController_protected.h"

#import "Album.h"
#import "AlbumTVCell.h"

#import "AlbumsViewModel.h"
#import "PhotosViewModel.h"

@interface AlbumsViewController () <UITableViewDelegate, UITableViewDataSource, AlbumsViewModelDelegate>

@property (nonatomic) UITableViewController * tvContoller;
@property (nonatomic) AlbumsViewModel * viewModel;
@property (nonatomic) UIRefreshControl * refreshControl;

@end

@implementation AlbumsViewController


#pragma mark - CommonVC

- (void)setupUI
{
    [super setupUI];
    
    self.title = @"Альбомы";

    [self setupTableViewController];
}


#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    
    if (self == nil) {
        return nil;
    }
    
    _viewModel = [AlbumsViewModel newWithDelegate:self];
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self fetchDataFromViewModel];
}


#pragma mark - <UITableViewDelegate>

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [[tableView dequeueReusableCellWithIdentifier:kReuseIdentifierTVCellAlbum forIndexPath:indexPath] apply:^(AlbumTVCell * cell) {
        cell.album = [self.viewModel albumAtIndex:indexPath.row];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.viewModel.numberOfAlbums;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    Album * album = [Album cast:[self.viewModel albumAtIndex:indexPath.row]];
    
    if (album.photos.count < 1) {
        return;
    }
    
    ThumbnailPhotosViewModel * viewModel = [ThumbnailPhotosViewModel newWithAlbum:album];
    [self.navigationController pushViewController:[PhotosViewController newWithViewModel:viewModel] animated:YES];
}


#pragma mark - <AlbumsViewModelDelegate>

- (void)albumsViewModelWillLoadAlbums
{
    [self showLoading];
}

- (void)albumsViewModelDidLoadAlbums
{
    [self.tvContoller.tableView reloadData];
    [self hideLoading];
    [self.refreshControl endRefreshing];
}


#pragma mark - 

- (void)setupTableViewController
{
    self.tvContoller = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    [self embedChildViewController:self.tvContoller toContainerView:self.view];
    
    self.tvContoller.tableView.delegate = self;
    self.tvContoller.tableView.dataSource = self;
    
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(fetchDataFromViewModel) forControlEvents:UIControlEventValueChanged];
    self.tvContoller.refreshControl = self.refreshControl;
    
    [self.tvContoller.tableView registerClass:AlbumTVCell.class forCellReuseIdentifier:kReuseIdentifierTVCellAlbum];
}

- (void)fetchDataFromViewModel
{
    [self.viewModel loadAlbums];
}


@end
