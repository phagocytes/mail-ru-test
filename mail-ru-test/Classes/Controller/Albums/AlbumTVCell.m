//
//  AlbumTVCell.m
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import "AlbumTVCell.h"
#import "Album.h"

NSString * const kReuseIdentifierTVCellAlbum = @"albumTVCell";

@interface AlbumTVCell () <AlbumDelegate>

@property (nonatomic) UIImageView * iconImageView;
@property (nonatomic) UILabel * titleLabel;
@property (nonatomic) UILabel * photosCountLabel;

@end


@implementation AlbumTVCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self == nil) {
        return nil;
    }
    
    UIView * container = self;
    
    self.iconImageView = [[UIImageView new] apply:^(UIImageView * iv) {
        iv.translatesAutoresizingMaskIntoConstraints = NO;
        
        [container addSubview:iv];
        
        NSDictionary * views = NSDictionaryOfVariableBindings(iv);
        CGSize size = albumThumbnailSize();
        NSDictionary * metrics = @{ @"width": @(size.width), @"height": @(size.height) };
        
        [container addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(10)-[iv(width)]" options:0 metrics:metrics views:views]];
        [container addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[iv(height)]" options:0 metrics:metrics views:views]];
        
        [container addConstraint:[NSLayoutConstraint constraintWithItem:container
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:iv
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.
                                                          constant:0.]];
    }];
    
    self.titleLabel = [[UILabel new] apply:^(UILabel * l) {
        l.translatesAutoresizingMaskIntoConstraints = NO;
        [container addSubview:l];
        
        NSDictionary * views = @{ @"iv": self.iconImageView, @"l": l };
        [container addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[iv]-(10)-[l]" options:0 metrics:nil views:views]];
        
        [container addConstraint:[NSLayoutConstraint constraintWithItem:container
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:l
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.
                                                          constant:0.]];
        
        [l setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];

    }];
    
    self.photosCountLabel = [[UILabel new] apply:^(UILabel * l) {
        l.translatesAutoresizingMaskIntoConstraints = NO;
        [container addSubview:l];
        
        NSDictionary * views = @{ @"tl": self.titleLabel, @"pl": l };
        [container addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[tl]-(>=10)-[pl]-(10)-|" options:0 metrics:nil views:views]];
        
        [container addConstraint:[NSLayoutConstraint constraintWithItem:container
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:l
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1.
                                                               constant:0.]];
        
        [l setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    }];
    
    return self;
}


#pragma mark - Setters/getters

- (void)setAlbum:(Album *)album
{
    if (_album == album) {
        return;
    }
    
    album.delegate = self;
    
    _album = album;
    [self updateUI];
}


#pragma mark - Update

- (void)updateUI
{
    [self updateTitle];
    [self updateImage];
    [self updatePhotosCount];
}

- (void)updatePhotosCount
{
    NSInteger photosCount = self.album.photos.count;
    self.photosCountLabel.text = photosCount < 1 ? @"-" : [NSString stringWithFormat:@"%d", (int)photosCount];
}

- (void)updateImage
{
    self.iconImageView.image = self.album.icon;
}

- (void)updateTitle
{
    self.titleLabel.text = self.album.title;
}


#pragma mark - AlbumDelegate

- (void)albumDidFinishFetchingIcon:(Album *)album
{
    [self updateImage];
}

- (void)albumDidFinishFetchingAssests:(Album *)album
{
    [self updatePhotosCount];
}

@end
