//
//  AlbumTVCell.h
//  mail-ru-test
//
//  Created by 5craft on 16/11/2016.
//
//

#import <UIKit/UIKit.h>

extern NSString * const kReuseIdentifierTVCellAlbum;

@class Album;

@interface AlbumTVCell : UITableViewCell

@property (nonatomic) Album * album;

@end
